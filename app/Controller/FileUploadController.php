<?php

App::uses('AppController', 'Controller', 'AppModel');

class FileUploadController extends AppController {

    public $uses = array('UploadedFile');

    public function index() {
        //The mail page is being displayed here
    }

    public function uploadFile() {
        $fileName = $_FILES["uploaded_file"]["name"]; // The file name
        $fileTmpLoc = $_FILES["uploaded_file"]["tmp_name"]; // File in the PHP tmp folder
        $fileType = $_FILES["uploaded_file"]["type"]; // The type of file it is
        $fileSize = $_FILES["uploaded_file"]["size"]; // File size in bytes
        $fileErrorMsg = $_FILES["uploaded_file"]["error"]; // 0 for false... and 1 for true
        $kaboom = explode(".", $fileName); // Split file name into an array using the dot
        $fileExt = end($kaboom); // Now target the last array element to get the file extension

        $message = '';

        // Place it into your "uploads" folder mow using the move_uploaded_file() function
        $moveResult = move_uploaded_file($fileTmpLoc, "uploads/$fileName");

        if (preg_match("/.(gif|jpg|png)$/i", $fileName)) {

            if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please browse for a file before clicking the upload button.";
                exit();
            } else if ($fileSize > 5242880) { // if file size is larger than 5 Megabytes
                echo "ERROR: Your file was larger than 5 Megabytes in size.";
                unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
                exit();
            } else if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
                echo "ERROR: An error occured while processing the file. Try again.";
                exit();
            }

            // Check to make sure the move result is true before continuing
            if ($moveResult != true) {
                echo "ERROR: File not uploaded. Try again.";
                unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
                exit();
            }

            list($w_orig, $h_orig) = getimagesize("uploads/$fileName");

            $uploadFolder = WWW_ROOT;
            $target_file = $uploadFolder . "/uploads/$fileName";

            if (($w_orig > 640) && ($h_orig > 480)) {

                $resized_file = $uploadFolder . "/uploads/640X480_$fileName";
                $compressedImage = $this->UploadedFile->imageCompression($target_file, $resized_file, 640, 480, $fileExt, "small");
            }

            if (($w_orig > 1280) && ($h_orig > 720)) {
                $resized_file_2 = $uploadFolder . "/uploads/1280X720_$fileName";
                $compressedImage1 = $this->UploadedFile->imageCompression($target_file, $resized_file_2, 1280, 720, $fileExt, "big");
            }
            if ($compressedImage || $compressedImage1) {
                $this->UploadedFile->saveImagePath($fileName, "640X480_" . $fileName, "1280X720_" . $fileName);
                $message = "The image is compressed and uploaded successfully";
            }
        } else {
            $message = "The file uploaded is not an image, hence the file is not compressed";
        }

        return $this->redirect(array('controller' => 'FileUpload', 'action' => 'listAll?message=' . $message));
        //Redirect them to the view list
    }

    public function listAll() {

        $allImages = $this->UploadedFile->getlist();
        $this->set('images', $allImages);
        $this->set('message', $_REQUEST['message']);
    }

    public function viewImage() {

        if (isset($_REQUEST)) {

            $this->set('images', $_REQUEST);
        }
    }

}
