<?php

App::uses('AppModel', 'Model', 'Imagick');

class UploadedFile extends AppModel {

    public $useTable = 'uploaded_file';
    
    
    public function getList($conditions = array()){
        
        $images = $this->find('all', array('conditions' => $conditions));
        // Returning images
        return $images;
        
    }

    /*
     * Function to upload Url
     */
    public function uploadFile($tmpFilePath, $fileName) {
        // Upload Folder
        $uploadFolder = WWW_ROOT;
        $storingPath = "uploads/";
        // Check if the file exists
        if (file_exists($uploadFolder . $storingPath . $fileName)) {

            $fileCounter = 0;
            while (file_exists($uploadFolder . $storingPath . $fileCounter . "_" . $fileName)) {
                $fileCounter++;
            }
            $newFilePath = $uploadFolder . $storingPath . $fileCounter . "_" . $fileName;
            $fileRelativeUrl = $storingPath . $fileCounter . "_" . $fileName;
        } else {
            $newFilePath = $uploadFolder . $storingPath . $fileName;
            $fileRelativeUrl = $storingPath . $fileName;
        }
        
        $response = array();
        //Makes sure we have a filepath
        if ($tmpFilePath != "") {
            
            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                // This will create a entry and return data with id and image url
                $this->create();
                $savedData = $this->save(array('url' => $fileRelativeUrl));
                $imagePath = $uploadFolder . $savedData['UploadedFile']['url'];
                //For Imagic
                $newResolution_640_480 =  $this->setResolution($imagePath, 640, 480);
                $newResolution_1280_720 =  $this->setResolution($imagePath, 640, 480);

                $updatedData = $this->updateAll(
                array('compressed_image_640_480' => "'" . $newResolution_640_480 . "'", 'compressed_image_1280_720' => "'" . $newResolution_1280_720 . "'"),
                array('id' => $savedData['id'])  //condition
                );
                return $savedData;
            }
        } else {
            // This will create a place-holder entry and return data with id and image url
            $this->create();
            return $this->save(array('url' => $fileRelativeUrl));
        }
    }
    
    public function imageCompression($target, $newcopy, $w, $h, $ext, $size) {
        list($w_orig, $h_orig) = getimagesize($target);
        $scale_ratio = $w_orig / $h_orig;

        $width = $w;
        $height = $h;
        if ($width > $height) {
            $y = 0;
            $x = ($width - $height) / 2;
            $smallestSide = $height;
            } else {
            $x = 0;
            $y = ($height - $width) / 2;
            $smallestSide = $width;
        }

        $img = "";
        $ext = strtolower($ext);
        if ($ext == "gif") {
            $img = imagecreatefromgif($target);
        } else if ($ext == "png") {
            $img = imagecreatefrompng($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        $compressedImage = imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
        imagejpeg($tci, $newcopy, 80);
        
        return true;
    }

    public function saveImagePath($target_file,$resized_file,$resized_file_2){
        $savedData = $this->save(array('url' => $target_file, 'compressed_image_640_480'=>$resized_file, 'compressed_image_1280_720' =>$resized_file_2));
        return $savedData;
    }
}
