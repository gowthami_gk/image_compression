<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>

    <div class="container">
        <center> <h2>Compressed images</h2></center>
        <div class="table-responsive">          
            <table class="table">
                <tbody>
                <div class="col-lg-12" >
                    <img id ="small_image" style ="display: none"  src= "/app/webroot/uploads/<?php echo $images['small_image'];?>"/></td>
                    <img id ="large_image" style ="display: none" src= "/app/webroot/uploads/<?php echo $images['large_image']; ?>"/></td>
                </div>
                </tbody>
            </table>
        </div>
    </div>
</html>

<script>

    windowSize = $(window).width();
    if (windowSize >= 1000) {
        $("#large_image").show();
    }
    if (windowSize <= 999 && windowSize >= 500) {
        $("#small_image").show();
    }
    if (windowSize < 499) {
        $("#small_image").show();
    }
</script>