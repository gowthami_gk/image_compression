<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <center><h2> All the images</h2></center>
            
            <?php 
            
            if(isset($message)){ ?>
                
            <center> <h2 style="color:green; margin-top: 30px;" ><b> <?php echo $message; ?></b></h2> </center>
            
            <?php 
            
            }
            ?>
            <div class="table-responsive">          
                <table class="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Original Image</th>
                        </tr>
                        (Please click on the image to view to check the different resolution )
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($images)) {
                            foreach ($images as $instanceImage) {
                                $imageData = $instanceImage['UploadedFile'];
                                ?>
                                <tr>
                            <div class="col-sm-4" >
                                <td><?php echo $imageData['id'] ?></td>
                            </div>
                            <div class="col-sm-4">
                                <td><a href="/FileUpload/viewImage?small_image=<?php echo $imageData['compressed_image_640_480'];?>&large_image=<?php echo $imageData['compressed_image_1280_720'];?>">
                                        <img src= "/app/webroot/uploads/<?php echo $imageData['url'];?>" width="150" height="150"/>
                                    </a>
                                </td>
                            </div>
                            <!--http://placehold.it/1920x1080 -->
                            </tr>
                        <?php }
                    } else {
                        ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>