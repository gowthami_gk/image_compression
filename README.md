
Currently, I have implemented a very basic version of the assignment. 

Please refer to the Imagecompresssion Document for the image reference.

For now I have kept very basic UI interaction as well. This can be made much better and more secured. 

The project flow is as follows.
App->Controller->FileUploadController(Logic Or the entry point is here) (Eg: Domain://FileUpload/index).

The application is talking to the database as per the MVC model on Model file. 

App->Model->UploadedFile. 

And also the database and the tables required for the project is as follows.

Table name -> uploaded_file

Table info:

CREATE TABLE `uploaded_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(360) DEFAULT NULL,
  `compressed_image_640_480` varchar(360) DEFAULT NULL,
  `compressed_image_1280_720` varchar(360) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

Project flow �> 

Domain/FileUpload -> Would take you to the upload page, after which all the images available will be shown. O�clock of the each images it is redirected to the the next page which displays the just image in different dimensions based on the screen size. 

Please do let me know if you have any doubts regarding the same. 

